class DegreeProgressionsController < ApplicationController
  before_action :set_degree_progression, only: [:show, :edit, :update, :destroy]

  # GET /degree_progressions
  # GET /degree_progressions.json
  def index
    @user = User.find(params[:user_id])
    @degree_progressions = DegreeProgression.where(:user_id => params[:user_id] )
  end

  # GET /degree_progressions/1
  # GET /degree_progressions/1.json
  def show
  end

  # GET /degree_progressions/new
  def new
    @degree_progression = DegreeProgression.new
  end

  # GET /degree_progressions/1/edit
  def edit
  end

  # POST /degree_progressions
  # POST /degree_progressions.json
  def create
  
    @degree_progression = DegreeProgression.new(degree_progression_params)
    
    
    respond_to do |format|
      if @degree_progression.save
        format.html { redirect_to @degree_progression, notice: 'Degree progression was successfully created.' }
        format.json { render :show, status: :created, location: @degree_progression }
        format.js { render :show, status: :created, location: @degree_progression }
      else
        format.html { render :new }
        format.json { render json: @degree_progression.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /degree_progressions/1
  # PATCH/PUT /degree_progressions/1.json
  def update
    respond_to do |format|
      
      if @degree_progression.update(degree_progression_params)
        format.html { redirect_to @degree_progression, notice: 'Degree progression was successfully updated.' }
        format.json { render :show, status: :ok, location: @degree_progression }
      else 
        format.html { render :edit }
        format.json { render json: @degree_progression.errors, status: :unprocessable_entity }
      end
    end
  end

  

  # DELETE /degree_progressions/1
  # DELETE /degree_progressions/1.json
  def destroy
    @degree_progression.destroy
    respond_to do |format|
      format.html { redirect_to degree_progressions_url, notice: 'Degree progression was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_degree_progression
      @degree_progression = DegreeProgression.find_by(:user => params[:user_id], :id => params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def degree_progression_params
      params.require(:degree_progression).permit(:user_id, :course_id, :is_complete, :student_id).merge(user_id: params[:user_id])
    end
end
