class UserMailer < ApplicationMailer
  default from: 'notifications@example.com'


  # Sends email to student user
  #
  def appointment_notification_email(user, appointment)
    a = Time.new.strftime('%Y%m%dT%H%M%S')
    @user = user
    @appointment = appointment
    # create a new temp file
    tempfile = Tempfile.new(['hello', '.ics'], Rails.root.join('tmp'))
    # basic outline for calendar event
    tempfile.write "BEGIN:VCALENDAR\n"
    tempfile.write "VERSION:2.0\n"
    tempfile.write "PRODID:-//hacksw/handcal//NONSGML v1.0//EN\n"
    tempfile.write "BEGIN:VEVENT\n"
    tempfile.write "UID:#{@user.id}#{@appointment.id}#{@appointment.start_time.sec}@example.com\n"
    tempfile.write "DTSTAMP:#{a}\n"
    tempfile.write "DTSTART:#{@appointment.start_time.strftime('%Y%m%dT%H%M%S')}\n"
    tempfile.write "DTEND:#{@appointment.end_time.strftime('%Y%m%dT%H%M%S')}\n"
    tempfile.write "SUMMARY:Meet with #{@appointment.advisor.first_name} for advising.\n"
    tempfile.write "END:VEVENT\n"
    tempfile.write "END:VCALENDAR\n"
    # close the file 
    tempfile.close
    # attach the file to the email
    attachments['appointment.ics'] = File.read(tempfile.path)
    
    mail(to: @user.email, subject: 'Scheduled appointment')
    # delete the file, we are done with it.
    tempfile.unlink
  end

  # Send email to advisor user.
  # 
  def appointment_notification_email_to_advisor(advisor, appointment)
    a = Time.new.strftime('%Y%m%dT%H%M%S')
    @user = advisor
    @appointment = appointment
    # create a new temp file
    tempfile = Tempfile.new(['hello', '.ics'], Rails.root.join('tmp'))
    # basic outline for calendar event
    tempfile.write "BEGIN:VCALENDAR\n"
    tempfile.write "VERSION:2.0\n"
    tempfile.write "PRODID:-//hacksw/handcal//NONSGML v1.0//EN\n"
    tempfile.write "BEGIN:VEVENT\n"
    tempfile.write "UID:#{@user.id}#{@appointment.id}#{@appointment.start_time.sec}@example.com\n"
    tempfile.write "DTSTAMP:#{a}\n"
    tempfile.write "DTSTART:#{@appointment.start_time.strftime('%Y%m%dT%H%M%S')}\n"
    tempfile.write "DTEND:#{@appointment.end_time.strftime('%Y%m%dT%H%M%S')}\n"
    tempfile.write "SUMMARY:Meeting with #{@appointment.student.first_name}\n"
    tempfile.write "END:VEVENT\n"
    tempfile.write "END:VCALENDAR\n"
    # close the file 
    tempfile.close
    # attach the file to the email
    attachments['appointment.ics'] = File.read(tempfile.path)
    mail(to: @user.email, subject: 'A student scheduled appointment with you')
    # delete the file, we are done with it.
    tempfile.unlink
  end


end
