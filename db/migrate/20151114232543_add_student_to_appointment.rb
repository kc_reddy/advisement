class AddStudentToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :student_id, :integer, references: :users
    add_column :appointments, :advisor_id, :integer, references: :users
  end
end
