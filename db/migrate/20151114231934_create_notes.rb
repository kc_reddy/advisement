class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.text :advisor_notes
      t.text :advisor_student_notes

      t.timestamps null: false
    end
  end
end
