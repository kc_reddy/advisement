# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151203055036) do

  create_table "appointments", force: :cascade do |t|
    t.integer  "note_id"
    t.datetime "start_time", null: false
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "student_id"
    t.integer  "advisor_id"
  end

  add_index "appointments", ["note_id"], name: "index_appointments_on_note_id"

  create_table "colleges", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string   "title"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "courses", ["department_id"], name: "index_courses_on_department_id"

  create_table "degree_progressions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "course_id"
    t.boolean  "is_complete"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "degree_progressions", ["course_id"], name: "index_degree_progressions_on_course_id"
  add_index "degree_progressions", ["user_id"], name: "index_degree_progressions_on_user_id"

  create_table "departments", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.integer  "college_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "departments", ["college_id"], name: "index_departments_on_college_id"

  create_table "majors", force: :cascade do |t|
    t.string   "name"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "majors", ["department_id"], name: "index_majors_on_department_id"

  create_table "notes", force: :cascade do |t|
    t.text     "advisor_notes"
    t.text     "advisor_student_notes"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "role_id", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "username",                        null: false
    t.string   "first_name",         default: "", null: false
    t.string   "last_name",          default: "", null: false
    t.string   "email",              default: ""
    t.integer  "sign_in_count",      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "major_id"
    t.integer  "minor_id"
    t.integer  "department_id"
  end

  add_index "users", ["department_id"], name: "index_users_on_department_id"
  add_index "users", ["major_id"], name: "index_users_on_major_id"
  add_index "users", ["minor_id"], name: "index_users_on_minor_id"
  add_index "users", ["username"], name: "index_users_on_username", unique: true

end
